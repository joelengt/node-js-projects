/*****************************************/
/*       PHASE 1 - BASIC SETUP: OK       */
/*     PHASE 2 - TODO HIGH : CURRENT     */
/*      PHASE 3 - TODO MEDIUM : WAIT     */
/*       PHASE 4 - TODO LOW : WAIT       */
/*****************************************/


/** SEEMS LIKE CODA 2 DOESN'T RECOGNIZE 'TODO:' AS TASKS **/
// Check if there's a way to fix this ***/
// PRIORITY: Low

// EXTRA: Beautiful colors
var colors = require('colors');

// INCLUDES
var SocketIOFileUploadServer = require("socketio-file-upload");
var _ = require('underscore');
var express = require("express");
var jade = require('jade');
var http = require('http');


// SETUP
var app = express().use(SocketIOFileUploadServer.router);
var server = http.createServer(app);
var io = require('socket.io').listen(server);



// GENERAL VARIABLES
var users = []; // user array


// SET VIEWS
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.set("view options", { layout: false });

// CONFIGURE IT!
app.configure(function() {
   app.use(express.static(__dirname + '/assets'));
   app.use(express.static(__dirname + '/uploaded/images'));
});


// TODO: Make different rooms
// PRIORITY: Medium


// TODO: Got to find a better way of routing
// PRIORITY: Medium
app.get('/', function(req, res){
    console.log("--- Routed to 'HOME'".green);
    res.render('home.jade');
});
// Route to CHAT
app.get('/chat', function(req, res){
    console.log("--- Routed to 'CHAT'".green);
    res.render('chat.jade');
});
// Route to UPLOAD
app.get('/upload', function(req, res){
    console.log("--- Routed to 'UPLOAD'".green);
    res.render('upload.jade');
});


// You better listen!
server.listen(8888, '0.0.0.0');


// Settup sockets to handle chat functions.
io.sockets.on('connection', function (socket) {

    socket.on('setUsername', function (data) {
       socket.set('username', data);      
       users.push( data );

       io.sockets.emit('user_connected', data);
       io.sockets.emit('users', users);
    });
    
    socket.on('disconnect', function () {
        socket.get('username', function (error, name) {
            if( name != null ) {
                // loop users, remove the disconnected user and emit new users array
                _.each( users, function(user, index){
                    if( user == name ) {
                        users = _.without( users, name );
                        
                        io.sockets.emit('users', users);
                    }
                });
                
                io.sockets.emit('user_disconnected', name);   
            }
        });
    });

// TODO: Should split CHAT + UPLOAD  methods 2 seperate classes. 
// PRIORITY: HIGH 
    // APPLIES TO CHAT
    socket.on('message', function (message) {
       socket.get('username', function (error, name) {
          var data = { 'message' : message, 'username' : name };
          
          // TODO: Change this to send to clients+sender
          // that way we can let the sender know from the callback if there's been an error sending his message
          // PRIORITY: Medium
          socket.broadcast.emit('message', data);
       });
    });
    
    
    // APPLIES TO IMAGES
    // TODO: add support for .png & .jpg
    // TODO: add support for .mp4? -> check in client which extension, if mp4, add a movie player.
    // PRIORITY: Medium
    // create uploader and listen to client socket
    var uploader = new SocketIOFileUploadServer();
    uploader.dir =  __dirname + "/uploaded/images"; // __dirname is neccesary in order to save the image, returns error otherwise!
    uploader.listen(socket);
    
    // ERROR
    uploader.on("error", function(event){
        console.log("Error from uploader", event .red);
    });
    
    // START UPLOAD
    uploader.on("start", function(event){
        console.log( "--- Server has started the upload!" .green );
    });
    // COMPLETE UPLOAD
    uploader.on("complete", function(event){
        console.log("--- Server has completed upload!" .green);
    });
    // SAVED FILE
    uploader.on("saved", function(event){
        console.log("--- Server has saved the file" .green);
        
        // TODO: Make this a seperate function / class (?)
        // PRIORITY: HIGH
        socket.get('username', function (error, name) {
            var data = { 'image' : event.file, 'username' : name };
            
            // SEND -> file to other clients
            io.sockets.emit('image-message', data );
        });
    });
});