var socket = io.connect();
var siofu = new SocketIOFileUpload(socket);
var currentScreen = '';
var username = '';

// Ready up!    
$(function() {
   initUpload();
   
   $(window).resize( positionElements );
});


// Boot it up!
function initUpload(){
    currentScreen = 'login';
    
    $("#uploadScreen").hide();
    
    $("#setUsername").on('mousedown', function() {setUsername()});
    
    $("#submit").click(function() {sendImage();});
    
    $(document).keypress(function(e) {
        if(e.which == 13) {
            if( currentScreen == 'login' ) {
                setUsername();
            } else if( currentScreen == 'upload' ) {
                sendImage();
            }
        }
    });
}

// Position all the elements
function positionElements(){
    if( currentScreen == 'login' ){
        // resize/restyle the login screen according to screen size?
    }
    
    if( currentScreen == 'upload' ){        
        var uploadHeight = $("#uploadSection").outerHeight();
    
        // calc height for chat entries
        var entriesHeight = $(window).outerHeight() - uploadHeight - 45; 
        
        // set height
        $("#shareSection").animate({height: entriesHeight }, 300);
    }
}

// When messages are displayed: scroll chat to bottom
function scrollBottom() {
    // if messages are higher than it's container, scroll chat window to newest message
    if( $("#shareSection").height() - $("#imageEntries").height() <= 0 ) {
        $("#shareSection").scrollTop( ($("#shareSection").height() - $("#imageEntries").height() - 10)*-1 );   
    }
}

// SET + SEND USERNAME to server
// GOTO upload screen
// TODO: Place this in a general.js file and use it for Chat + Upload
// PRIORITY: Medium
function setUsername() {
   if ($("#usernameInput").val() != "") {
      socket.emit('setUsername', $("#usernameInput").val());
      
      $('#loginScreen').fadeOut('fast', function(){
        $('#uploadScreen').fadeIn('fast', function() {
            currentScreen = 'upload';
            
            username = $("#usernameInput").val();
            
            setupUpload();
            
            positionElements();
        });
      });
   }
}

// SETUP -> listen to file upload events and send to server (SocketIOFileUpload takes care of sending it to the server.)
function setupUpload() {
     // input field
     siofu.listenOnInput(document.getElementById("file_input"));
     // drag&drop field
     siofu.listenOnDrop(document.getElementById("file_drop"));
     // upload btn
     document.getElementById("file_button").addEventListener("click", siofu.prompt, false);
}

// RECIEVE message from server
socket.on('image-message', function(data) {
   addImage(data);
});

// ADD message to chat
function addImage(data) {
    if( username == data.username ) {
        $("#imageEntries").append('<div class="message image own"><strong class="username">' + data.username  + ' - ' + getCurrentTime() + '</strong><div class="cleaner">&nbsp;</div><img src="' + data.image.base + '.png"></div>');        
    } else {
        $("#imageEntries").append('<div class="message image"><strong class="username">' + getCurrentTime() + ' - ' + data.username  + '</strong><div class="cleaner">&nbsp;</div><img src="' + data.image.base + '.png"></div>');
    }
    
    
    $(".message img").load(function(){
        scrollBottom();
    })
    
    // TODO: Place this in a general.js file and use it for Chat + Upload
    // PRIORITY: Medium
    
    
    setTimeout(function(){
    scrollBottom();
    }, 200);
}


// TODO: Place this in a general.js file and use it for Chat + Upload
// PRIORITY: Medium
function getCurrentTime(){
    var now = new Date();
    var outStr = now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();
    
    return outStr;
}