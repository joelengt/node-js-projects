var socket = io.connect();

var currentScreen = '';
var showElements = false;


// Ready up!    
$(function() {
   initChat();
   
   $(window).resize( positionElements );
});


// Boot it up!
function initChat(){
    currentScreen = 'login';
    
    $("#chatScreen").hide();
    
    $("#setUsername").on('mousedown', function() {setUsername()});
    
    $("#submit").click(function() {sendMessage();});
    
    $(document).keypress(function(e) {
        if(e.which == 13) {
            if( currentScreen == 'login' ) {
                setUsername();
            } else if( currentScreen == 'chat' ) {
                sendMessage();
            }
        }
    });
}

// Position all the elements
function positionElements(){

    if( currentScreen == 'login' ){
        // resize/restyle the login screen according to screen size?
    }
    
    if( currentScreen == 'chat' ){
        if( !showElements ) {
            $("#chatScreen").fadeIn('fast');
            $("#chatBox").fadeIn('fast');
        }
        
        var msgBoxHeight = $("#msgSection").outerHeight();
    
        // calc height for chat entries
        var chatScreenHeight = $(window).outerHeight() - msgBoxHeight - 45; 
        
        // set height
        $("#chatSection").css({'height':  chatScreenHeight });
        
        // animate chat sections
        if( !showElements ) {
            $("#chatScreen #msgSection").css({'top': $("#chatScreen msgSection").outerHeight() + 200 });
            $("#chatScreen #msgSection").show().animate({
              top: 0
            }, 200);
    
            $("#chatScreen #userSection").css({'left':$("#chatScreen #userSection").outerWidth()});
            $("#chatScreen #userSection").show().animate({
              left: 0
            }, 200);
            
            showElements = true;
        }
    }
}

// When messages are displayed: scroll chat to bottom
function scrollBottom() {
    // if chat messages are higher than it's container, scroll chat window to newest message
    if( $("#chatSection").height() - $("#chatEntries").height() <= 0 ) {
        $("#chatSection").scrollTop( ($("#chatSection").height() - $("#chatEntries").height() - 10)*-1 );   
    }
}


// SET + SEND USERNAME to server
// GOTO chat screen
// TODO: Place this in a general.js file and use it for Chat + Upload
// PRIORITY: Medium
function setUsername() {
   if ($("#usernameInput").val() != "") {
      socket.emit('setUsername', $("#usernameInput").val());
      
      $('#loginScreen').fadeOut('fast', function(){
        $('#chatScreen').fadeIn('fast', function() {
            currentScreen = 'chat';
            positionElements();
        });
      });
   }
}

// SEND message to server
// ADD message to own screen.
// TODO: send your own messages to server first? 
    // This way we can check if the sender still has a connection, if not: display error, if so: display message.
function sendMessage() {
   if ($('#messageInput').val() != "") {
      socket.emit('message', $('#messageInput').val());
      addMessage($('#messageInput').val(), "Me");
      
      $('#messageInput').val('').focus();
   }
}
// RECIEVE message from server
socket.on('message', function(data) {
   addMessage(data['message'], data['username']);
});
// ADD message to chat
function addMessage(msg, username) {
    if( username == "Me" ) {
        $("#chatEntries").append('<div class="message own"><p>' + getCurrentTime() + ' - ' + username + ' : ' + msg + '</p></div>');         
    } else {
        $("#chatEntries").append('<div class="message"><p>' + getCurrentTime() + ' - ' + username + ' : ' + msg + '</p></div>');
    }
    
    scrollBottom();
}


// USER CONNECTED -> display on user list
socket.on('user_connected', function(data) {
    var now = getCurrentTime();
    $("#chatEntries").append('<div class="connected"><p>' + now + ': ' + data + ' has connected</p></div>');
    
    $("#user-list").append('<li class="user-item">- '+ data +'</li>');
    
    scrollBottom();
});
// USER DISCONNECTED -> remove from user list
socket.on('user_disconnected', function(data) {
    var now = getCurrentTime();
    $("#chatEntries").append('<div class="disconnected"><p>' + now + ': ' + data + ' has disconnected</p></div>');
    
    // TODO: Place this in a general.js file and use it for Chat + Upload
    // PRIORITY: Medium
    scrollBottom();
});

// whenever there's a new user, display his name on the user-list
socket.on('users', function(users) {
    $("#user-list").empty();
    
    $.each( users, function(index,value){
        $("#user-list").append('<li class="user-item">- '+ value +'</li>');
    });
});

// TODO: Place this in a general.js file and use it for Chat + Upload
// PRIORITY: Medium
function getCurrentTime(){
    var now = new Date();
    var outStr = now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();
    
    return outStr;
}
